;;; org-yaap-tests.el --- Tests for org-yaap -*- lexical-binding: t -*-

(require 'org-yaap)

(defun org-yaap-tests-get-heading (custom-id)
  "Get the heading with CUSTOM-ID from 'org-yaap-tests.org'."
  (find-file "tests/org-yaap-tests.org")
  (let ((inhibit-message t))
    (org-link-open-from-string (format "[[%s]]" custom-id)))
  (org-element-at-point))

(ert-deftest org-yaap-should-alert-before-0 ()
  "Should alert if the timestamp is exactly equal to the scheduled
  time."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#alert-before-0")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 23)))))

(ert-deftest org-yaap-should-not-alert-before-0 ()
  "Should not alert if the timestamp is not equal to the scheduled
  time."
  (should (not (org-yaap--should-alert
                (org-yaap-tests-get-heading "#alert-before-0")
                '(timestamp
                  (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 24))))))

(ert-deftest org-yaap-custom-alert-before ()
  "Should alert if the timestamp matches the ALERT_BEFORE
property of the heading."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#custom-alert-before")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 2)))))

(ert-deftest org-yaap-ignore-scheduled ()
  "If `org-yaap-include-scheduled' is nil, don't alert for
scheduled headings."
  (let ((org-yaap-include-scheduled nil))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#ignore-scheduled")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-ignore-deadline ()
  "If `org-yaap-include-deadline' is nil, don't alert for
headings with a deadline."
  (let ((org-yaap-include-deadline nil))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#ignore-deadline")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-todo-only ()
  "Ignore headings without a todo if `org-yaap-todo-only' is
non-nil."
  (let ((org-yaap-todo-only t))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#todo-only")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-include-done ()
  "Should alert for a completed heading if
`org-yaap-exclude-done' is nil."
  (let ((org-yaap-exclude-done nil))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#include-done")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-exclude-done ()
  "Should not alert for a completed heading if
`org-yaap-exclude-done is non-nil."
  (should (not (org-yaap--should-alert
                (org-yaap-tests-get-heading "#exclude-done")
                '(timestamp
                  (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-only-tags-should-alert ()
  "Should alert if heading has a tag in `org-yaap-only-tags'."
  (let ((org-yaap-only-tags '("alert")))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#only-tags-should-alert")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-only-tags-should-not-alert ()
  "Should not alert if heading does not have a tag in
`org-yaap-only-tags'."
  (let ((org-yaap-only-tags '("alert")))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#only-tags-should-not-alert")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-daily-alert ()
  "Should alert for a heading without time specified if the
timestamp is exactly 9am."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#daily-alert")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 0)))))

(ert-deftest org-yaap-custom-alert-time ()
  "Should alert at ALERT_TIME for a heading without a time
specified."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#alert-time")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 19 :minute-start 20)))))

(ert-deftest org-yaap-overdue-interval ()
  "Should alert every 30 minutes after a heading is due."
  (let ((heading (org-yaap-tests-get-heading "#overdue-interval")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 30))))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 0))))))

(ert-deftest org-yaap-overdue-list ()
  "Should alert for each element in ALERT_OVERDUE."
  (let ((heading (org-yaap-tests-get-heading "#overdue-list")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 13))))
    (should (org-yaap--should-alert
            heading
            '(timestamp
              (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 34))))))
